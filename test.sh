#!/bin/sh
tf="/tmp/sha3test"
dd if="/dev/urandom" of="$tf" bs=1 count=10000
for i in `seq 1 10000`; do
	a=`head -c $i "$tf" | ./sha3`
	b=`head -c $i "$tf" | openssl dgst -sha3-512`
	if [ "(stdin)= $a" != "$b" ]; then
		1>&2 echo "Mismatch!"
		1>&2 echo "  $a"
		1>&2 echo "  $b"
		exit 1
	fi
done
