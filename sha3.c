#include <stdint.h>
#include <stddef.h> /* size_t */
#include <string.h> /* memset() */
#include <stdio.h>

#define BUFSZ 4096
#define MDSZ 64

typedef struct {
	uint64_t h[25];
	int mdlen, off;
} sha3_state_t;

static uint64_t rol64(uint64_t a, uint64_t b) {
	return (a << b) | (a >> ((64 - b) & 0x3F)); /* Shifting right by 64 bits breaks clang. */
}

static void keccak_f1600(uint64_t a[25]) {
	const uint64_t rc[24] = {
		0x0000000000000001, 0x0000000000008082, 0x800000000000808a, 0x8000000080008000,
		0x000000000000808b, 0x0000000080000001, 0x8000000080008081, 0x8000000000008009,
		0x000000000000008a, 0x0000000000000088, 0x0000000080008009, 0x000000008000000a,
		0x000000008000808b, 0x800000000000008b, 0x8000000000008089, 0x8000000000008003,
		0x8000000000008002, 0x8000000000000080, 0x000000000000800a, 0x800000008000000a,
		0x8000000080008081, 0x8000000000008080, 0x0000000080000001, 0x8000000080008008
	};
	const int r[25] = {
		 0,  1, 62, 28, 27,
		36, 44,  6, 55, 20,
		 3, 10, 43, 25, 39,
		41, 45, 15, 21,  8,
		18,  2, 61, 56, 14
	};
	int i;

	for (i = 0; i < 24; ++i) {
		uint64_t b[25];
		int x, y;

		/* Theta step. */
		for (x = 0; x < 5; ++x)
			b[x] = a[x + 0 * 5] ^ a[x + 1 * 5] ^ a[x + 2 * 5] ^ a[x + 3 * 5] ^ a[x + 4 * 5];
		for (x = 0; x < 5; ++x) {
			uint64_t c = b[(x + 4) % 5] ^ rol64(b[(x + 1) % 5], 1);
			for (y = 0; y < 5; ++y)
				a[x + y * 5] ^= c;
		}

		/* Rho and Pi steps. */
		for (x = 0; x < 5; ++x)
			for (y = 0; y < 5; ++y)
				b[y + (x * 2 + y * 3) % 5 * 5] = rol64(a[x + y * 5], r[x + y * 5]);

		/* Chi step. */
		for (x = 0; x < 5; ++x)
			for (y = 0; y < 5; ++y)
				a[x + y * 5] = b[x + y * 5] ^ ((~b[(x + 1) % 5 + y * 5]) & b[(x + 2) % 5 + y * 5]);

		/* Iota step. */
		a[0] ^= rc[i];
	}
}

void sha3_init(sha3_state_t *s, int mdlen) {
	memset(s->h, 0, sizeof(s->h));
	s->mdlen = mdlen;
	s->off = 0;
}

void sha3_update(sha3_state_t *s, const uint8_t *msg, size_t len) {
	while (len--) {
		s->h[s->off / 8] ^= (uint64_t) *msg++ << (s->off % 8 * 8);
		if (++s->off == 200 - 2 * s->mdlen) {
			keccak_f1600(s->h);
			s->off = 0;
		}
	}
}

void sha3_finish(sha3_state_t *s, uint8_t *md) {
	int i;
	s->h[s->off / 8] ^= (uint64_t) 0x06 << (s->off % 8 * 8);
	s->h[(200 - 2 * s->mdlen - 1) / 8] ^= (uint64_t) 0x80 << ((200 - 2 * s->mdlen - 1) % 8 * 8);
	keccak_f1600(s->h);
	for (i = 0; i < s->mdlen; ++i)
		md[i] = s->h[i / 8] >> (i % 8 * 8);
}

int main() {
	static uint8_t buf[BUFSZ];
	size_t len;
	sha3_state_t s;
	uint8_t md[MDSZ];
	int i;

	sha3_init(&s, MDSZ);
	while (!feof(stdin)) {
		len = fread(buf, 1, sizeof(buf), stdin);
		sha3_update(&s, buf, len);
		if (ferror(stdin)) {
			fprintf(stderr, "Failure while reading stdin.\n");
			return 1;
		}
	}
	sha3_finish(&s, md);

	for (i = 0; i < MDSZ; ++i)
		printf("%02x", md[i]);
	printf("\n");

	return 0;
}
