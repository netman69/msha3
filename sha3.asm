; nasm -f elf64 sha3.asm -o sha3.o; brandelf -t FreeBSD sha3.o; ld -o sha3 sha3.o; ./sha3

bits 64

%define buf_len (4096)
%define md_len (64)
%define in_len (200 - 2 * md_len)

section .bss
align 16

alignb 16
buf resb buf_len
md_hex resb md_len * 2 + 1

alignb 16
tmp resq 25

alignb 16
md resq 25

section .data
align 16

rc dq 0x0000000000000001, 0x0000000000008082, 0x800000000000808a, 0x8000000080008000,\
      0x000000000000808b, 0x0000000080000001, 0x8000000080008081, 0x8000000000008009,\
      0x000000000000008a, 0x0000000000000088, 0x0000000080008009, 0x000000008000000a,\
      0x000000008000808b, 0x800000000000008b, 0x8000000000008089, 0x8000000000008003,\
      0x8000000000008002, 0x8000000000000080, 0x000000000000800a, 0x800000008000000a,\
      0x8000000080008081, 0x8000000000008080, 0x0000000080000001, 0x8000000080008008
hex db '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
err db "Error reading stdin.", 0x0a
err_len equ $ - err

section .text
align 16

keccak:
	xor r12, r12 ; Round counter.
.round:
	; Theta step.
;	for (x = 0; x < 5; ++x)
;		b[x] = a[x + 0 * 5] ^ a[x + 1 * 5] ^ a[x + 2 * 5] ^ a[x + 3 * 5] ^ a[x + 4 * 5];
	mov rax, [md + (0 +  0) * 8]
	xor rax, [md + (0 +  5) * 8]
	xor rax, [md + (0 + 10) * 8]
	xor rax, [md + (0 + 15) * 8]
	xor rax, [md + (0 + 20) * 8]
	mov rbx, rax

	mov rax, [md + (1 +  0) * 8]
	xor rax, [md + (1 +  5) * 8]
	xor rax, [md + (1 + 10) * 8]
	xor rax, [md + (1 + 15) * 8]
	xor rax, [md + (1 + 20) * 8]
	mov rcx, rax

	mov rax, [md + (2 +  0) * 8]
	xor rax, [md + (2 +  5) * 8]
	xor rax, [md + (2 + 10) * 8]
	xor rax, [md + (2 + 15) * 8]
	xor rax, [md + (2 + 20) * 8]
	mov rdx, rax

	mov rax, [md + (3 +  0) * 8]
	xor rax, [md + (3 +  5) * 8]
	xor rax, [md + (3 + 10) * 8]
	xor rax, [md + (3 + 15) * 8]
	xor rax, [md + (3 + 20) * 8]
	mov rsi, rax

	mov rax, [md + (4 +  0) * 8]
	xor rax, [md + (4 +  5) * 8]
	xor rax, [md + (4 + 10) * 8]
	xor rax, [md + (4 + 15) * 8]
	xor rax, [md + (4 + 20) * 8]
	mov rdi, rax

;	for (x = 0; x < 5; ++x) {
;		uint64_t c = b[(x + 4) % 5] ^ rol64(b[(x + 1) % 5], 1);
;		for (y = 0; y < 5; ++y)
;			a[x + y * 5] ^= c;
;	}
	mov rax, rcx
	rol rax, 1
	xor rax, rdi
	xor [md + (0 + 0 * 5) * 8], rax
	xor [md + (0 + 1 * 5) * 8], rax
	xor [md + (0 + 2 * 5) * 8], rax
	xor [md + (0 + 3 * 5) * 8], rax
	xor [md + (0 + 4 * 5) * 8], rax

	mov rax, rdx
	rol rax, 1
	xor rax, rbx
	xor [md + (1 + 0 * 5) * 8], rax
	xor [md + (1 + 1 * 5) * 8], rax
	xor [md + (1 + 2 * 5) * 8], rax
	xor [md + (1 + 3 * 5) * 8], rax
	xor [md + (1 + 4 * 5) * 8], rax

	mov rax, rsi
	rol rax, 1
	xor rax, rcx
	xor [md + (2 + 0 * 5) * 8], rax
	xor [md + (2 + 1 * 5) * 8], rax
	xor [md + (2 + 2 * 5) * 8], rax
	xor [md + (2 + 3 * 5) * 8], rax
	xor [md + (2 + 4 * 5) * 8], rax

	mov rax, rdi
	rol rax, 1
	xor rax, rdx
	xor [md + (3 + 0 * 5) * 8], rax
	xor [md + (3 + 1 * 5) * 8], rax
	xor [md + (3 + 2 * 5) * 8], rax
	xor [md + (3 + 3 * 5) * 8], rax
	xor [md + (3 + 4 * 5) * 8], rax

	mov rax, rbx
	rol rax, 1
	xor rax, rsi
	xor [md + (4 + 0 * 5) * 8], rax
	xor [md + (4 + 1 * 5) * 8], rax
	xor [md + (4 + 2 * 5) * 8], rax
	xor [md + (4 + 3 * 5) * 8], rax
	xor [md + (4 + 4 * 5) * 8], rax

	; Rho and Pi steps.
;	for (x = 0; x < 5; ++x)
;		for (y = 0; y < 5; ++y)
;			b[y + (x * 2 + y * 3) % 5 * 5] = rol64(a[x + y * 5], r[x + y * 5]);
	mov rax, [md + (0 + 0 * 5) * 8]
	mov [tmp + 0 * 8], rax

	mov rax, [md + (0 + 1 * 5) * 8]
	rol rax, 36
	mov [tmp + 16 * 8], rax

	mov rax, [md + (0 + 2 * 5) * 8]
	rol rax, 3
	mov [tmp + 7 * 8], rax

	mov rax, [md + (0 + 3 * 5) * 8]
	rol rax, 41
	mov [tmp + 23 * 8], rax

	mov rax, [md + (0 + 4 * 5) * 8]
	rol rax, 18
	mov [tmp + 14 * 8], rax

	mov rax, [md + (1 + 0 * 5) * 8]
	rol rax, 1
	mov [tmp + 10 * 8], rax

	mov rax, [md + (1 + 1 * 5) * 8]
	rol rax, 44
	mov [tmp + 1 * 8], rax

	mov rax, [md + (1 + 2 * 5) * 8]
	rol rax, 10
	mov [tmp + 17 * 8], rax

	mov rax, [md + (1 + 3 * 5) * 8]
	rol rax, 45
	mov [tmp + 8 * 8], rax

	mov rax, [md + (1 + 4 * 5) * 8]
	rol rax, 2
	mov [tmp + 24 * 8], rax

	mov rax, [md + (2 + 0 * 5) * 8]
	rol rax, 62
	mov [tmp + 20 * 8], rax

	mov rax, [md + (2 + 1 * 5) * 8]
	rol rax, 6
	mov [tmp + 11 * 8], rax

	mov rax, [md + (2 + 2 * 5) * 8]
	rol rax, 43
	mov [tmp + 2 * 8], rax

	mov rax, [md + (2 + 3 * 5) * 8]
	rol rax, 15
	mov [tmp + 18 * 8], rax

	mov rax, [md + (2 + 4 * 5) * 8]
	rol rax, 61
	mov [tmp + 9 * 8], rax

	mov rax, [md + (3 + 0 * 5) * 8]
	rol rax, 28
	mov [tmp + 5 * 8], rax

	mov rax, [md + (3 + 1 * 5) * 8]
	rol rax, 55
	mov [tmp + 21 * 8], rax

	mov rax, [md + (3 + 2 * 5) * 8]
	rol rax, 25
	mov [tmp + 12 * 8], rax

	mov rax, [md + (3 + 3 * 5) * 8]
	rol rax, 21
	mov [tmp + 3 * 8], rax

	mov rax, [md + (3 + 4 * 5) * 8]
	rol rax, 56
	mov [tmp + 19 * 8], rax

	mov rax, [md + (4 + 0 * 5) * 8]
	rol rax, 27
	mov [tmp + 15 * 8], rax

	mov rax, [md + (4 + 1 * 5) * 8]
	rol rax, 20
	mov [tmp + 6 * 8], rax

	mov rax, [md + (4 + 2 * 5) * 8]
	rol rax, 39
	mov [tmp + 22 * 8], rax

	mov rax, [md + (4 + 3 * 5) * 8]
	rol rax, 8
	mov [tmp + 13 * 8], rax

	mov rax, [md + (4 + 4 * 5) * 8]
	rol rax, 14
	mov [tmp + 4 * 8], rax

	; Chi step.
;	for (x = 0; x < 5; ++x)
;		for (y = 0; y < 5; ++y)
;			a[x + y * 5] = b[x + y * 5] ^ ((~b[(x + 1) % 5 + y * 5]) & b[(x + 2) % 5 + y * 5]);
	mov rax, [tmp + (1 + 0 * 5) * 8]
	not rax
	and rax, [tmp + (2 + 0 * 5) * 8]
	xor rax, [tmp + (0 + 0 * 5) * 8]
	mov [md + (0 + 0 * 5) * 8], rax

	mov rax, [tmp + (1 + 1 * 5) * 8]
	not rax
	and rax, [tmp + (2 + 1 * 5) * 8]
	xor rax, [tmp + (0 + 1 * 5) * 8]
	mov [md + (0 + 1 * 5) * 8], rax

	mov rax, [tmp + (1 + 2 * 5) * 8]
	not rax
	and rax, [tmp + (2 + 2 * 5) * 8]
	xor rax, [tmp + (0 + 2 * 5) * 8]
	mov [md + (0 + 2 * 5) * 8], rax

	mov rax, [tmp + (1 + 3 * 5) * 8]
	not rax
	and rax, [tmp + (2 + 3 * 5) * 8]
	xor rax, [tmp + (0 + 3 * 5) * 8]
	mov [md + (0 + 3 * 5) * 8], rax

	mov rax, [tmp + (1 + 4 * 5) * 8]
	not rax
	and rax, [tmp + (2 + 4 * 5) * 8]
	xor rax, [tmp + (0 + 4 * 5) * 8]
	mov [md + (0 + 4 * 5) * 8], rax

	mov rax, [tmp + (2 + 0 * 5) * 8]
	not rax
	and rax, [tmp + (3 + 0 * 5) * 8]
	xor rax, [tmp + (1 + 0 * 5) * 8]
	mov [md + (1 + 0 * 5) * 8], rax

	mov rax, [tmp + (2 + 1 * 5) * 8]
	not rax
	and rax, [tmp + (3 + 1 * 5) * 8]
	xor rax, [tmp + (1 + 1 * 5) * 8]
	mov [md + (1 + 1 * 5) * 8], rax

	mov rax, [tmp + (2 + 2 * 5) * 8]
	not rax
	and rax, [tmp + (3 + 2 * 5) * 8]
	xor rax, [tmp + (1 + 2 * 5) * 8]
	mov [md + (1 + 2 * 5) * 8], rax

	mov rax, [tmp + (2 + 3 * 5) * 8]
	not rax
	and rax, [tmp + (3 + 3 * 5) * 8]
	xor rax, [tmp + (1 + 3 * 5) * 8]
	mov [md + (1 + 3 * 5) * 8], rax

	mov rax, [tmp + (2 + 4 * 5) * 8]
	not rax
	and rax, [tmp + (3 + 4 * 5) * 8]
	xor rax, [tmp + (1 + 4 * 5) * 8]
	mov [md + (1 + 4 * 5) * 8], rax

	mov rax, [tmp + (3 + 0 * 5) * 8]
	not rax
	and rax, [tmp + (4 + 0 * 5) * 8]
	xor rax, [tmp + (2 + 0 * 5) * 8]
	mov [md + (2 + 0 * 5) * 8], rax

	mov rax, [tmp + (3 + 1 * 5) * 8]
	not rax
	and rax, [tmp + (4 + 1 * 5) * 8]
	xor rax, [tmp + (2 + 1 * 5) * 8]
	mov [md + (2 + 1 * 5) * 8], rax

	mov rax, [tmp + (3 + 2 * 5) * 8]
	not rax
	and rax, [tmp + (4 + 2 * 5) * 8]
	xor rax, [tmp + (2 + 2 * 5) * 8]
	mov [md + (2 + 2 * 5) * 8], rax

	mov rax, [tmp + (3 + 3 * 5) * 8]
	not rax
	and rax, [tmp + (4 + 3 * 5) * 8]
	xor rax, [tmp + (2 + 3 * 5) * 8]
	mov [md + (2 + 3 * 5) * 8], rax

	mov rax, [tmp + (3 + 4 * 5) * 8]
	not rax
	and rax, [tmp + (4 + 4 * 5) * 8]
	xor rax, [tmp + (2 + 4 * 5) * 8]
	mov [md + (2 + 4 * 5) * 8], rax

	mov rax, [tmp + (4 + 0 * 5) * 8]
	not rax
	and rax, [tmp + (0 + 0 * 5) * 8]
	xor rax, [tmp + (3 + 0 * 5) * 8]
	mov [md + (3 + 0 * 5) * 8], rax

	mov rax, [tmp + (4 + 1 * 5) * 8]
	not rax
	and rax, [tmp + (0 + 1 * 5) * 8]
	xor rax, [tmp + (3 + 1 * 5) * 8]
	mov [md + (3 + 1 * 5) * 8], rax

	mov rax, [tmp + (4 + 2 * 5) * 8]
	not rax
	and rax, [tmp + (0 + 2 * 5) * 8]
	xor rax, [tmp + (3 + 2 * 5) * 8]
	mov [md + (3 + 2 * 5) * 8], rax

	mov rax, [tmp + (4 + 3 * 5) * 8]
	not rax
	and rax, [tmp + (0 + 3 * 5) * 8]
	xor rax, [tmp + (3 + 3 * 5) * 8]
	mov [md + (3 + 3 * 5) * 8], rax

	mov rax, [tmp + (4 + 4 * 5) * 8]
	not rax
	and rax, [tmp + (0 + 4 * 5) * 8]
	xor rax, [tmp + (3 + 4 * 5) * 8]
	mov [md + (3 + 4 * 5) * 8], rax

	mov rax, [tmp + (0 + 0 * 5) * 8]
	not rax
	and rax, [tmp + (1 + 0 * 5) * 8]
	xor rax, [tmp + (4 + 0 * 5) * 8]
	mov [md + (4 + 0 * 5) * 8], rax

	mov rax, [tmp + (0 + 1 * 5) * 8]
	not rax
	and rax, [tmp + (1 + 1 * 5) * 8]
	xor rax, [tmp + (4 + 1 * 5) * 8]
	mov [md + (4 + 1 * 5) * 8], rax

	mov rax, [tmp + (0 + 2 * 5) * 8]
	not rax
	and rax, [tmp + (1 + 2 * 5) * 8]
	xor rax, [tmp + (4 + 2 * 5) * 8]
	mov [md + (4 + 2 * 5) * 8], rax

	mov rax, [tmp + (0 + 3 * 5) * 8]
	not rax
	and rax, [tmp + (1 + 3 * 5) * 8]
	xor rax, [tmp + (4 + 3 * 5) * 8]
	mov [md + (4 + 3 * 5) * 8], rax

	mov rax, [tmp + (0 + 4 * 5) * 8]
	not rax
	and rax, [tmp + (1 + 4 * 5) * 8]
	xor rax, [tmp + (4 + 4 * 5) * 8]
	mov [md + (4 + 4 * 5) * 8], rax

	; Iota step.
;	a[0] ^= rc[i];
	mov rax, [rc + r12 * 8]
	xor [md], rax

	inc r12
	cmp r12, 24
	jne .round
	ret

global _start
_start:
	; Read input and do hashing.
	xor r13, r13 ; Offset counter.
.rin:
	mov rdi, 0 ; stdin
	mov rsi, buf
	mov rdx, buf_len
	mov rax, 3 ; SYS_read
	syscall
	jc .err
	cmp rax, 0
	je .end
	mov r15, rax ; Counter of remaining bytes.
	mov r14, buf ; Pointer to data being hashed.
.lop:
	mov al, [r14]
	xor [md + r13], al
	inc r13
	inc r14
	dec r15
	cmp r13, in_len
	jne .nxt
	call keccak
	xor r13, r13
.nxt:
	test r15, r15
	jnz .lop
	jmp .rin
.end:

	; Do the padding thing and final hashing.
	xor byte [md + r13], 0x06
	xor byte [md + in_len - 1], 0x80
	call keccak

	; Write output.
	mov rcx, md_len
	xor rdx, rdx
.hlp:
	mov al, [md + rdx]
	and rax, 0x0F
	mov al, [hex + rax]
	mov [md_hex + rdx * 2 + 1], al
	mov al, [md + rdx]
	shr al, 4
	and rax, 0x0F
	mov al, [hex + rax]
	mov [md_hex + rdx * 2], al
	inc rdx
	loop .hlp
	mov byte [md_hex + md_len * 2], 0x0a
	mov rdi, 1 ; stdout
	mov rsi, md_hex
	mov rdx, md_len * 2 + 1
	mov rax, 4 ; SYS_write
	syscall

	; Exit.
	xor rdi, rdi
	mov rax, 1 ; SYS_exit
	syscall

	; Stdin has failed us.
.err:
	mov rdi, 2 ; stderr
	mov rsi, err
	mov rdx, err_len
	mov rax, 4 ; SYS_write
	syscall
	mov rdi, 1
	mov rax, 1 ; SYS_exit
	syscall
